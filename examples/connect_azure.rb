# Create authentication objects
require "azure_mgmt_iot_hub"
require "pp"

include Azure::ARM::IotHub
include Azure::ARM::IotHub::Models

tenant_id = "112cef06-c9e3-4d50-b78a-6b4525cb291e" 
client_id = "http://95effc3d-c2c8-47fd-92c4-e641687438fa"
secret = "SnBs7HdrWv66Wt"
token_provider = MsRestAzure::ApplicationTokenProvider.new(tenant_id, client_id, secret)
credentials = MsRest::TokenCredentials.new(token_provider)

# Create a client - a point of access to the API and set the subscription id
client = IotHubClient.new(credentials)
path = '/devices'
options = {
  query_params: {'api-version' => '2016-02-01', 'top' => 20}
}

data = client.make_request(:get, path, options)
resource = IotHubResource.new(client)
pp(data)

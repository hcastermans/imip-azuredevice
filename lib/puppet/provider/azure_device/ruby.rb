require 'json'

require_relative '../../../puppet_x/itility/azure_provider'

Puppet::Type.type(:azure_device).provide(:ruby, parent: PuppetX::Itility::AzureIoTProvider) do

  @doc = "Manages a azure device"
  commands :azureiot_twins   => '/usr/local/bin/azureiot_twins'
  commands :azureiot_desired => '/usr/local/bin/azureiot_twins'

  mk_resource_methods

  def self.convert_rest_to_puppet(json)
    device = {} 
    device[:ensure] = :present
    device[:name] = json['deviceId']
    properties = json['properties']

    device[:metadata] = properties['desired']['$metadata']
    device[:desired_properties] = properties['desired']
    device[:desired_properties].delete('$metadata')
    device[:desired_properties].delete('$version')
    
    device[:reported_properties] = properties['reported']
    new(device)
  end
  
  def self.convert_puppet_to_rest(resource, property_hash)
    data = {} 
    data['metadata'] = resource[:metadata]
    data
  end

  def self.all_resources()
    output = azureiot_twins()
    JSON.parse(output)
  end

  def update_resource
    puts resource[:name]

  end

end

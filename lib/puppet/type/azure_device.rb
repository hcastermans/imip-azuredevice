
Puppet::Type.newtype(:azure_device) do
  ensurable


  newparam(:name, namevar: true) do
    desc 'The unique id of the device.'
    validate do |value|
      raise 'Name should be a string' unless value.is_a? String
    end
  end

  newproperty(:metadata) do
    desc 'metadata'
    validate do |value|
      raise 'Type should be a hash' unless value.is_a? Hash
    end
  end

  newproperty(:desired_properties) do
    desc 'To-be delivered properties'
    validate do |value|
      raise 'Type should be a hash' unless value.is_a? Hash
    end
  end

  newproperty(:reported_properties) do
    desc 'Current properties'
    validate do |value|
      raise 'Type should be a hash' unless value.is_a? Hash
    end
  end

end

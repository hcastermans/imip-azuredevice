module PuppetX
  module Itility
    class AzureIoTProvider < Puppet::Provider
      DEBUG_PREFIX = '[Rest AzureIoT provider] '.freeze

      def initialize(value = {})
        super(value)
        Puppet.debug(DEBUG_PREFIX + 'initialize')
        @property_flush = {}
      end

      def exists?
        Puppet.debug(DEBUG_PREFIX + resource[:name] + ' exists?')
        @property_hash[:ensure] == :present
      end

      def create
        Puppet.debug(DEBUG_PREFIX + 'Create ' + resource[:name])
        @property_flush[:ensure] = :present
      end

      def destroy
        Puppet.debug(DEBUG_PREFIX + 'Destroy ' + resource[:name])
        @property_flush[:ensure] = :absent
      end

      def flush
        Puppet.debug(DEBUG_PREFIX + 'Flush ' + resource[:name])

        if @property_flush[:ensure] == :absent
          delete_resource
        elsif @property_flush[:ensure] == :present
          create_resource
        else
          update_resource
        end
        @property_hash = resource.to_hash
      end

      def self.prefetch(resources)
        instances.each do |prov|
          resource = resources[prov.name]
          resource.provider = prov if resource
        end
      end

      def self.instances
        Puppet.debug(DEBUG_PREFIX + 'Instances called')
        all_resources().collect do |resource|
          convert_rest_to_puppet(resource)
        end.flatten
      end

      private

      def self.all_resources()
        [{'DeviceId' => 'Raspberrypi01'}]
      end

      def resource_id
        resource[:name]
      end

      def create_resource
        Puppet.debug(DEBUG_PREFIX + 'Creating resource for ' + resource[:name])
      end

      def delete_resource
        Puppet.debug(DEBUG_PREFIX + 'Deleting resource for ' + resource[:name])
      end

      def update_resource
        Puppet.debug(DEBUG_PREFIX + 'Updating resource for ' + resource[:name])
      end
    end
  end
end

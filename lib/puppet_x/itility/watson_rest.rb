require 'puppet/util/feature'
require 'open-uri'
require 'rest-client'
Puppet.features.add(:restclient, libs: 'rest-client')

module PuppetX
  module Itility
    class WatsonIoTRestClient
      DEBUG_PREFIX  = '[Watsion IoT] '.freeze
      PATH_PREFIX   = 'api/v0002/'.freeze

      def self.delete_request(path, item)
        self.initialize
        encoded_item = URI.encode(item)
        url = @watsonapi_base_url + PATH_PREFIX + path + encoded_item
        request = RestClient::Request.new(
          method:     :delete,
          verify_ssl: @verify_ssl,
          url:        url,
          user:       @watsonapi_apikey,
          password:   @watsonapi_token
         )

        do_request(request)
      end

      def self.get_request_item(path, item)
        encoded_item = URI.encode(item)
        get_request(path + encoded_item)
      end

      def self.get_request(path)
        self.initialize
        url = @watsonapi_base_url + PATH_PREFIX + path
        request = RestClient::Request.new(
          method:     :get,
          verify_ssl: @verify_ssl,
          url:        url,
          user:       @watsonapi_apikey,
          password:   @watsonapi_token)

        do_request(request)
      end

      def self.post_request(path, postdata)
        self.initialize
        url = @watsonapi_base_url + PATH_PREFIX + path
        puts "Path ",url, " postdata: ",postdata
        request = RestClient::Request.new(
          method:     :post,
          verify_ssl: @verify_ssl,
          url:        url,
          payload:    postdata.to_json,
          user:       @watsonapi_apikey,
          password:  @watsonapi_token,
          headers: { content_type: 'application/json'}
            )
        do_request(request)
      end

      def self.put_request(path, item, putdata)
        self.initialize
        encoded_item = URI.encode(item)
        url = @watsonapi_base_url + PATH_PREFIX + path + encoded_item
        request = RestClient::Request.new(
          method:     :put,
          verify_ssl: @verify_ssl,
          url:        url,
          payload:    putdata.to_json,
          user:       @watsonapi_apikey,
          password:  @watsonapi_token, 
          headers: { content_type: 'application/json'}
          )
        puts url
        do_request(request)
      end

      def self.move_request(path, item, other_item, should_move_before)
        self.initialize
        encoded_item = URI.encode(item.to_s)
        encoded_other_item = URI.encode(other_item.to_s)
        action_string = '?action=move&' + (should_move_before ? 'before' : 'after') + '='
        uri = @watsonapi_base_url + PATH_PREFIX + path + encoded_item + action_string + encoded_other_item
        request = RestClient::Request.new(
          method:     :put,
          verify_ssl: @verify_ssl,
          url:        uri,
          payload:    action_string + other_item.to_s,
          user:       @watsonapi_apikey,
          password:   @watsonapi_token
          )

        do_request(request)
      end

      private


      def self.do_request(request)
        self.initialize
        Puppet.debug(DEBUG_PREFIX + 'performing ' + request.method.upcase + ' request at ' + request.url)
        request.execute
      rescue RestClient::Exception => e
        Puppet.debug(DEBUG_PREFIX + 'Error during HTTP request, error type: RestClient::Exception')
        raise Puppet::Error, DEBUG_PREFIX + 'Error with FortiGate HTTP request, error was: ' + e.inspect
      rescue SocketError => e
        Puppet.debug(DEBUG_PREFIX + 'Error during HTTP request, error type: SocketError')
        raise Puppet::Error, DEBUG_PREFIX + 'Socket error while communicating with FortiGate: ' + e.inspect
      end

      def self.initialize
        Puppet.debug(DEBUG_PREFIX + 'Create FortiGateRestClient')
        # @watsonapi_host     = ENV['FORTIGATE_HOST']
        #@watsonapi_user     = ENV['FORTIGATE_USER']
        #@watsonapi_password = ENV['FORTIGATE_PASSWORD']
        #@verify_ssl         = ENV['FORTIGATE_INSECURE'].nil? ? OpenSSL::SSL::VERIFY_PEER : OpenSSL::SSL::VERIFY_NONE
       @watsonapi_org="5q196f"
       #@watsonapi_type="Simulator"
       @watsonapi_apikey='a-5q196f-nlwhxjztxz'
       @watsonapi_token='usm(N340ZcjD4J&a(c'
       @watsonapi_verify_ssl = false
       if @watsonapi_org.nil? || @watsonapi_apikey.nil? || @watsonapi_token.nil? 
          raise Puppet::Error, 'You must provide WATSONAPI_HOST, WATSONAPI_USER and WATSONAPI_PASSWORD as environment variables.'
        end

        @watsonapi_base_url = 'https://' + @watsonapi_org + '.internetofthings.ibmcloud.com/'
      end

      def self.finalize
          Puppet.debug(DEBUG_PREFIX + 'Destroy')
          logout
      end

      # Ensure the session with the FortiGate is released (e.g. logging out) when this client class is destroyed.
      ObjectSpace.define_finalizer(self, proc { finalize } )
    end
  end
end
